package com.example.myfirstapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.StackView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StackView stackView = findViewById(R.id.stack_view);
        TestAdapter adapter = new TestAdapter(getLayoutInflater());
        stackView.setAdapter(adapter);
    }
}
