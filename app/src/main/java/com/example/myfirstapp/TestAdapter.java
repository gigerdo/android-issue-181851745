package com.example.myfirstapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class TestAdapter extends BaseAdapter implements Adapter {
    private final LayoutInflater inflater;
    private final ArrayList<String> tasks;

    public TestAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        this.tasks = new ArrayList<>();
        this.tasks.add("Task 1");
        this.tasks.add("Task 2");
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public String getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.task_card, parent, false);
            TextView textView = view.findViewById(R.id.task_text);

            // Exception happens here: getView is called even though getCount() returns 0
            String taskText = tasks.get(position);
            textView.setText(taskText);

            Button button = view.findViewById(R.id.task_done_button);
            button.setOnClickListener(v -> {
                tasks.remove(position);
                notifyDataSetChanged();
            });
        }

        return view;
    }
}